.PHONY: all deploy-env docker-deploy-env docker-deploy-main install clean .confirm

DOCKER_COMPOSE	:= "$(shell which docker-compose)"
SERVICES_ENV	:= photo_db rabbitmq cache
SERVICES_MAIN	:= hack_service

all: docker-deploy-env

docker-deploy-env:
	$(DOCKER_COMPOSE) up $(SERVICES_ENV)

docker-deploy-main:
	$(DOCKER_COMPOSE) up $(SERVICES_MAIN)

install:
	mkdir logs
	@poetry install
	@poetry shell

clean: .confirm
	@git clean -xfd -e config.env \
					  -e .idea
	@if [ -n "$$(poetry env list)" ]; then \
	 poetry env remove python; \
	fi

.confirm:
	@while [ -z "$$CONTINUE" ]; do \
	 read -r -p "Вы действительно хотите продолжить? [y/N] " CONTINUE; \
	 if [ "$$CONTINUE" != "y" ] && [ "$$CONTINUE" != "Y" ]; then \
	  echo "Прерывание."; \
	  exit 1; \
	 fi; \
	done

