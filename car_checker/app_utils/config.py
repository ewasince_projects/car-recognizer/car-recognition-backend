import os

from pydantic import BaseModel


class Config(BaseModel):
    """
    Основной класс конфига, выполняющий проверку всех полей
    """

    def __new__(cls):
        if hasattr(cls, "_instance"):
            return cls._instance

        # создание конфига
        config_dict = {}

        for param in cls.model_fields:
            param: str
            var = os.environ.get(param.upper())
            if var is not None:
                config_dict[param] = var

        cls._instance = super(Config, cls).__new__(cls)
        super(Config, cls._instance).__init__(**config_dict)

        return cls._instance

    # noinspection PyMissingConstructor
    def __init__(self, *args, **kwargs):
        pass

    ####################

    # secrets

    # public config
    log_level_console: str = "INFO"
    log_level_file: str = "DEBUG"
    log_file: str = "logs/service.log"
    keep_log_files: str = "14"

    service_host: str = "127.0.0.1"
    service_port: int = 43222

    temp_dir: str = "tmp"
    photos_livetime: int = 3600 * 24 * 2  # in seconds

    debug_mode: bool = False
