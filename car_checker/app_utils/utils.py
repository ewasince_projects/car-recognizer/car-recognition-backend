"""Here will be different utilities"""

import os.path
import uuid
from typing import Union

from car_checker.app_utils.config import Config


def get_temp_subdir(config: Config, subdir=""):
    return os.path.join(config.temp_dir, subdir)


def get_filename(
    config: Config, photo_uuid: Union[uuid.UUID, str], directory="photos", ext="png"
):
    if not ext.startswith("."):
        ext = f".{ext}"
    file_name_template = "%(uuid)s%(ext)s"
    file_name_template = os.path.join(
        get_temp_subdir(config, directory), file_name_template
    )
    return file_name_template % {"uuid": photo_uuid, "ext": ext}
