import io
import logging
import os.path
import shutil
import urllib.parse
import uuid
from collections import defaultdict

import cv2
import fastapi
from classy_fastapi import Routable, get, post
from fastapi import File, UploadFile
from models.experimental import attempt_load
from starlette.responses import FileResponse, JSONResponse, StreamingResponse
from starlette.templating import Jinja2Templates
from utils.torch_utils import select_device

from car_checker.app_utils.config import Config
from car_checker.app_utils.utils import get_filename, get_temp_subdir
from car_checker.errors import PhotoNotFound
from car_detection_lib.detect import run

LOG = logging.getLogger(__name__)


class Api(Routable):
    def __init__(
        self,
    ):
        super().__init__()
        self.config = Config()

    @get("/")
    async def index(self, request: fastapi.Request):
        return templates.TemplateResponse("index.html", {"request": request})

    @post("/recognize/")
    async def recognize_photo(self, file: UploadFile = File(...)):
        photo_uuid = str(uuid.uuid4())
        filename_in = get_filename(self.config, photo_uuid)
        filename_out_txt = get_filename(
            self.config, photo_uuid, os.path.join("results", "labels"), ext="txt"
        )

        with open(filename_in, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        file.file.close()

        weights = "car_detection_lib/runs/train/exp12/weights/best.pt"
        run(
            weights=weights,
            source=filename_in,
            project=get_temp_subdir(self.config, "results"),
            name="",
            exist_ok=True,
            save_txt=True,
        )
        device = select_device("")
        model = attempt_load(weights, map_location=device)  # load FP32 model
        names = (
            model.module.names if hasattr(model, "module") else model.names
        )  # get class names
        new_names = []
        for name in names:
            match name:
                case "Car":
                    res = "Машина"
                case "Motorcycle":
                    res = "Мотоцикл"
                case "Truck":
                    res = "Грузовик"
                case "Bus":
                    res = "Автобус"
                case "Bicycle":
                    res = "Велосипед"
                case _:
                    res = ""
            new_names.append(res)
        names = new_names

        print(names)

        found_classes = defaultdict(lambda: 0)

        with open(filename_out_txt) as f:
            for s in f.readlines():
                class_number = s.split(" ")[0]
                found_classes[class_number] += 1

        if not found_classes:
            description = "Ничего не найдено"
        else:
            description = "Найдено "
            object_strings = []
            for class_num, count in found_classes.items():
                class_name = names[int(class_num)]
                object_strings.append(pluralize(count, class_name))
            description += ", ".join(object_strings)

        return {
            "photo_id": urllib.parse.quote(photo_uuid),
            "description": description,
        }

    @get("/detected_photo/{photo_uuid}", response_class=FileResponse)
    async def detected_photo(self, photo_uuid: str = None):
        photo_filename = get_filename(self.config, photo_uuid, "results")
        if not os.path.exists(photo_filename):
            return JSONResponse(
                content={"message": "Resource Not Found"}, status_code=404
            )

        try:
            img = cv2.imread(photo_filename)
        except Exception:
            raise PhotoNotFound(photo_uuid)

        res, im_png = cv2.imencode(".png", img)
        return StreamingResponse(io.BytesIO(im_png.tobytes()), media_type="image/png")


def pluralize(number, word):
    # morph = pymorphy2.MorphAnalyzer()
    # parsed_word = morph.parse(word)[0]
    #
    # if number % 10 == 1 and number % 100 != 11:
    #     form = parsed_word.inflect({'nomn'})  # Именительный падеж, единственное число
    # elif 2 <= number % 10 <= 4 and (number % 100 < 10 or number % 100 >= 20):
    #     form = parsed_word.inflect({'gent'})  # Родительный падеж, единственное число
    # else:
    #     form = parsed_word.inflect({'plur', 'gent'})  # Родительный падеж, множественное число
    #
    # return f'{number} {form.word if form else word}'
    return f"{number} {word}"


templates = Jinja2Templates(
    "frontend/templates",
    context_processors=None,
)
