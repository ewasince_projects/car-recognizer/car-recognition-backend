# service exceptions
from fastapi import HTTPException


class PhotoNotFound(HTTPException):
    def __init__(self, uuid: str):
        super().__init__(
            status_code=404,
            detail=f"photo {uuid} not found",
        )
