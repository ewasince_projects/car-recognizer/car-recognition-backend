import asyncio
import logging
import os
import signal
import sys
import time
import traceback
from logging import handlers
from threading import Thread

import schedule
import uvicorn
from fastapi import FastAPI
from starlette.staticfiles import StaticFiles

from car_checker.api.rest import Api
from car_checker.app_utils.config import Config

config = Config()

logger = logging.getLogger("")
logger.setLevel(logging.DEBUG)
format = logging.Formatter(
    "%(filename)17s[LINE:%(lineno)3d]# %(levelname)-6s [%(asctime)s]  %(message)s"
)

console_handler = logging.StreamHandler(sys.stdout)
console_handler.setFormatter(format)
console_handler.setLevel(config.log_level_console)
logger.addHandler(console_handler)

if not os.path.exists(config.log_file.split("/")[0]):
    os.mkdir(config.log_file.split("/")[0])

if not os.path.exists(config.log_file):
    with open(config.log_file, "w") as f:
        f.write("")

file_handler = handlers.RotatingFileHandler(
    config.log_file,
    backupCount=int(config.keep_log_files),
    encoding=None,
    delay=False,
    maxBytes=5 * 1024 * 1024,
)
file_handler.setFormatter(format)
file_handler.setLevel(config.log_level_file)
logger.addHandler(file_handler)

logging.getLogger("aiormq").setLevel(logging.WARNING)
logging.getLogger("aio_pika").setLevel(logging.WARNING)

tmp_dirs = ["photos", "videos"]

for tmp_dir in tmp_dirs:
    directory = os.path.join(config.temp_dir, tmp_dir)

    if not os.path.exists(directory):
        os.makedirs(directory)

DEBUG = True


def cleanup_temp_dir():
    for root, dirs, files in os.walk(config.temp_dir):
        if not files:
            continue

        for file in files:
            abs_path = os.path.join(root, file)
            t = os.path.getmtime(abs_path)
            is_old_photo = time.time() - t > config.photos_livetime
            if is_old_photo:
                os.remove(abs_path)


def run_jobs():
    while True:
        schedule.run_pending()
        time.sleep(1)


async def main():
    # set up jobs
    t = Thread(target=run_jobs, daemon=True)
    t.start()

    schedule.every(1).hours.do(cleanup_temp_dir)

    # run_jobs()

    api = Api()

    app = FastAPI()
    app.mount("/static", StaticFiles(directory="frontend/static"), name="static")
    app.mount("/js", StaticFiles(directory="frontend/js"), name="js")
    app.mount("/lib", StaticFiles(directory="frontend/lib"), name="lib")
    app.include_router(api.router)

    serv_config = uvicorn.Config(
        app=app,
        host=config.service_host,
        port=config.service_port,
        timeout_graceful_shutdown=0 if config.debug_mode else 5,  # TODO: Move to config
    )
    server = uvicorn.Server(serv_config)

    await server.serve()


if __name__ == "__main__":
    logger.info("### START ###")

    logger.info(f"Приложение запущено с параметрами: \n{config.model_dump()}")

    loop = asyncio.get_event_loop()

    try:
        loop.run_until_complete(main())
    except Exception as e:
        logger.critical(f"Critical fail: {e}")
        logger.critical(f"{traceback.print_exc()}")
        sys.exit(1)
    finally:
        logger.debug("TRY GRACEFUL SHUTDOWN")
        # shutil.rmtree(config.temp_dir)
        logger.debug("clean dir")
        for task in asyncio.all_tasks(loop):
            logger.debug(f"clean task {task.get_name()}")
            task.cancel()
        logger.debug(f"clean all tasks")
        loop.stop()
        logger.debug(f"loop stop")
        loop.close()
        logger.debug(f"loop close")
        if config.debug_mode:
            os.kill(os.getpid(), signal.SIGILL)
    logger.info("### STOP ###")
