from dataclasses import dataclass

from PIL.Image import Image


@dataclass
class PhotoMetadata:
    photo_UUID: str
    video_UUID: str
    ts: int
    vsecond: int


@dataclass
class QueryProcessPhotoDto:
    image: Image
    metadata: PhotoMetadata
