from dataclasses import dataclass

from pydantic import BaseModel

from car_checker_lib.query_process_photo_dto import PhotoMetadata


class Box(BaseModel):
    coords: tuple[float, float, float, float]
    cls_: str


class MLResult(BaseModel):
    result: bool
    boxes: list[Box]


@dataclass
class ResultProcessPhotoDto:
    result: MLResult
    metadata: PhotoMetadata
