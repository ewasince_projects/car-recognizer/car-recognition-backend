FROM nvcr.io/nvidia/pytorch:24.03-py3

ENV PYTHONUNBUFFERED=1 \
  PIP_NO_CACHE_DIR=off \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  POETRY_VERSION=1.7.0 \
  PYTHONPATH=/app

WORKDIR /app

# Install linux packages
RUN apt update && \
    apt install -y zip htop screen libgl1-mesa-glx

# Install python dependencies
COPY requirements_api.txt .
RUN pip install --no-cache -r requirements_api.txt

COPY requirements_docker.txt .
RUN pip install --no-cache -r requirements_docker.txt

RUN pip install --no-cache opencv-python==4.8.0.74

# Copy contents
COPY . /app
## Creating folders, and files for a project:
#COPY car_checker ./hack_backend
ENV PYTHONPATH=$PYTHONPATH:/app/car_detection_lib

CMD python car_checker/main.py