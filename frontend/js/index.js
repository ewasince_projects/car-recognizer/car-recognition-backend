/* globals $ */
$(function () {
    const base_photo_url = "/detected_photo",
        stream_name = currentPath = window.location.pathname.split('/').pop()

    const detectedPhotosJson = localStorage.getItem("detectedPhotos"),
        detectedPhotosModel = detectedPhotosJson ? JSON.parse(detectedPhotosJson) : []

    detectedPhotosModel.forEach(drawNewPhoto)

    $("#clear").click(function (e) {
        e.preventDefault();
        console.log("clear")
        detectedPhotosModel.splice(0, detectedPhotosModel.length)
        localStorage.setItem("detectedPhotos", JSON.stringify(detectedPhotosModel)) // save photoUuidModel
        $("#detectedPhotos").empty()
    })

    // upload photo
    $('#uploadFileForm').submit(function (e) {
        e.preventDefault();
        const uploadFile = $("#fileInput").prop('files')[0]

        if (!uploadFile) {
            showNotification("Выберите файл", success = false)
            return
        }

        var formData = new FormData();
        formData.append("file", uploadFile);


        $.ajax({
            url: '/recognize/',
            type: 'POST',
            data: formData,
            contentType: false, // Это указывает jQuery не устанавливать заголовок Content-Type
            processData: false, // Это предотвращает преобразование данных в строку запроса
            success: function (data) {
                console.log()
                // data type:
                // {
                // photo_id: string
                // description: string
                // }

                addPhotoUuidToModel(data)
                drawNewPhoto(data)
                showNotification("Файл загружен успешно!")
            },
            error: function (xhr, status, error) {
                showNotification("Файл не загружен!", success = false)
            }
        });
    });

    function addPhotoUuidToModel(photoData) {
        detectedPhotosModel.push(photoData) // add photoUuid to photoUuidModel
        localStorage.setItem("detectedPhotos", JSON.stringify(detectedPhotosModel)) // save photoUuidModel
    }

    function drawNewPhoto(photoData) {
        // draw  new photo object to list
        const detectedPhotoElement = $("#detectedPhoto").clone(),
            detectedPhotoUrl = buildPhotoUrl(photoData.photo_id),
            detectedPhotosElement = $("#detectedPhotos");
        console.log(detectedPhotoUrl)

        detectedPhotoElement.removeClass("d-none")

        detectedPhotoElement.find(".img").each(function () {
            thisElement = $(this)
            if (thisElement.prop('tagName') === "A") {
                thisElement.attr("href", detectedPhotoUrl)
                thisElement.attr("data-lightbox", "detected-photos")
            } else {
                thisElement.attr("src", detectedPhotoUrl)
            }
        })
        detectedPhotoElement.find(".desc").each(function () {
            thisElement = $(this)
            if (thisElement.prop('tagName') === "A") {
                thisElement.attr("data-title", photoData.description)
            } else if (thisElement.prop('tagName') === "IMG") {
                thisElement.attr("alt", photoData.description)
            } else {
                thisElement.text(photoData.description)
            }
        })

        detectedPhotoElement.find(".btn-dislike").on("click", function (e){
            console.log("data", photoData)
            e.preventDefault();
            detectedPhotoElement.remove()

            const index = detectedPhotosModel.indexOf(photoData);
            console.log("index", index)
            if (index !== -1) {
                detectedPhotosModel.splice(index, 1);
            }
            localStorage.setItem("detectedPhotos", JSON.stringify(detectedPhotosModel)) // save photoUuidModel
        })

        // append new photo object to list
        detectedPhotosElement.append(detectedPhotoElement)

    }

    function buildPhotoUrl(uuid) {
        return base_photo_url + '/' + uuid
    }

    function showNotification(message, success = true) {
        const notify_class = success ? "notification-success" : "notification-fail"
        $('#notification')
            .stop(true, false)
            .setClass(notify_class)
            .text(message)
            .fadeIn(500)
            .delay(3000)
            .fadeOut(500);
    }


    $.fn.setClass = function (classes) {
        this.attr('class', classes);
        return this;
    };
})


